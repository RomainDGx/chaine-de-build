const request = require('supertest');
const { expect } = require('chai');
const app = require('../../app.js');

function connectToExpress() {
  const agent = request.agent(app);
  return agent;

}

describe('GET / api/items', () => {

  it('Shound return all items in database', async () => {
    const agent = connectToExpress();
  
    return agent.get('/api/items')
      .expect(200)
      .expect('Content-Type', /json/)
      .then(res => {
        expect(res.body)
          .to.be.an('array')
          .of.length(2);
        
        expect(res.body[0]).to.have.property('id');
      });
  });
});
