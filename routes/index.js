var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.status(200).send(`<!DOCTYPE html>
  <head>
    <meta charset="utf-8"/>
    <title>Hello World !</title>
  </head>
  <body>
    <h1>Hello World</h1>
  </body>
</html>`
  );
});

router.get('/api/items', (req, res) => {
  res.json([
    {
      id: 1,
      description: "This is a description"
    },
    {
      id: 2,
      description: "This is another desciption"
    }
  ]);
});

module.exports = router;
